<?php
$quicklinks_json_url = "https://gitlab.com/dodod/dodod/raw/master/quicklinks.json";
$default_url = "/homepage";

$json = file_get_contents($quicklinks_json_url);
$obj = json_decode($json, true);
$quicklinks = $obj["links"];

$path = $_SERVER["REQUEST_URI"];
if ($path === "/") {
    header("Location: " . $default_url);
    die();
}

foreach ($quicklinks AS $short => $link) {
    if (strpos($short, ",")) {
        $aliases = explode(",", $short);
        foreach ($aliases AS $alias) {
            if ("/" . $alias === $path) {
                header("Location: " . $link);
                die();
            }
        }
    }
    if ("/" . $short === $path) {
        header("Location: " . $link);
        die();
    }
}


// redirect to oh14, if the link exists there...
function get_http_response_code($theURL) {
    $headers = get_headers($theURL);
    return substr($headers[0], 9, 3);
}

$oh14link = "https://oh14.de/" . substr($path, 1);
if (get_http_response_code($oh14link) != "404") {
    header("Location: " . $oh14link);
    die();
}


// convert to objects
$quicklinksForSorting = [];
foreach ($quicklinks AS $short => $link) {
    $temp = new stdClass();
    $temp->short = $short;
    $temp->link = $link;
    array_push($quicklinksForSorting, $temp);
}

function cmp($a, $b)
{
    global $path;

    $sim1 = similar_text($path, $a->short);
    $sim2 = similar_text($path, $b->short);
    if ($sim1 == $sim2) {
        return 0;
    }
    return ($sim1 > $sim2) ? -1 : 1;
}

http_response_code(404);
usort($quicklinksForSorting, "cmp");
?>

<html>
<head>
</head>
<body>
<h1>Quicklink nicht gefunden...</h1>
<p>Wir haben folgende Quicklinks (sortiert nach Matchgenauigkeit):</p>
<ul>
    <?php foreach ($quicklinksForSorting AS $quicklink): ?>
        <li><a href="<?php echo $quicklink->link ?>"><?php echo $quicklink->short ?></a></li>
    <?php endforeach; ?>
    <ul>
</body>
</html>
