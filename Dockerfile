FROM php:apache
MAINTAINER Lukas Mertens <info@dodod.de>

RUN a2enmod rewrite

COPY src /var/www/html
COPY .htaccess /var/www/html
